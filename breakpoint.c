#include <sys/ptrace.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sys/user.h>
#include <sys/uio.h>
#include <sys/syscall.h>
#include <linux/elf.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <asm/ptrace.h>
 
int got_signal = 0;
siginfo_t last_info;
 
void print_regs(struct user_regs_struct regs)
{
    for (int i = 0; i < 32 ; i++) {
        printf("%016lx  ", regs.regs[i]);
        if (!((i + 1) % 8))
            putchar('\n');
    }
    printf("pc: %016x\n\n", regs.pc);
}
 
void sig_action(int sig, siginfo_t *info, void *ucontext)
{
    last_info = *info;
    __sync_synchronize();
    got_signal = 1;
}
 
int main()
{
    pid_t child;
    struct user_regs_struct regs;
    struct iovec iov = {
        .iov_base = &regs,
        .iov_len = sizeof (struct user_regs_struct),
    };
    int a = 1, b = 1;
    const struct sigaction act = {
        .sa_sigaction = sig_action,
        .sa_flags = SA_SIGINFO | SA_NOCLDWAIT,
    };
    int wstatus;
    pid_t pid = getpid();
 
    child = fork();
 
    if (child == 0) {
        // we can't get registers reliably from the parent process
        // without this ptrace(PTRACE_TRACEME) call in the child
        ptrace(PTRACE_TRACEME, 0, NULL, NULL);
        while (b < 6) {
            int *c = &a;
            printf("child: %d\n", b); fflush(stdout);
            *c = 1;
            b += a;
            sleep(1);
            __asm__ __volatile__("brk #0");
        }
 
        return 0;
    }
 
    sigaction(SIGCHLD, &act, NULL);
 
    do {
        while (!got_signal)
            usleep(20000);
        got_signal = 0;
        waitpid(child, &wstatus, WNOHANG);
 
        if (last_info.si_code == CLD_EXITED)
            return 0;
 
        // si_code codes in /usr/include/asm-generic/siginfo.h
        printf("sig: %d [si_code=%d si_status=%d]\n", last_info.si_signo,
            last_info.si_code, last_info.si_status); fflush(stdout);
 
        ptrace(PTRACE_GETREGSET, child, NT_PRSTATUS, &iov);
        print_regs(regs); fflush(stdout);
 
        regs.pc += 4;
        ptrace(PTRACE_SETREGSET, child, NT_PRSTATUS, &iov);
 
        ptrace(PTRACE_CONT, child, NULL, NULL);
    } while (1);
}